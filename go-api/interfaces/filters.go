package interfaces

type Filters interface {
	passes() bool
	message() string
}
