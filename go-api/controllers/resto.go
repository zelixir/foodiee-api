package controllers

import (
	"github.com/foodie-api/go-api/model"

	"github.com/jmoiron/sqlx"
)

type RestoController struct{}

func (resto RestoController) Find(index int, db *sqlx.DB) (model.Restaurant, error) {
	restoObj := model.Restaurant{}
	err := db.Get(&restoObj, `SELECT * FROM Restaurant WHERE id = ? `, index)
	if err != nil {
		return restoObj, err
	}
	return restoObj, err
}
func (resto RestoController) FindAll(db *sqlx.DB) ([]model.Restaurant, error) {
	restos := []model.Restaurant{}
	err := db.Select(&restos, "SELECT * FROM Restaurant")
	return restos, err
}
func (resto RestoController) Create(restoObj model.Restaurant, db *sqlx.DB) (int64, error) {
	tx := db.MustBegin()
	result, err := tx.NamedExec(`INSERT INTO Restaurant (manager_firstname , manager_lastname, address, Postal_Code, email,phone_number,password ,country, city ,name, created_at,total_orders) VALUES ( :manager_firstname , :manager_lastname, :address, :Postal_Code, :email,:phone_number,:password ,:country, :city ,:name, :created_at,:total_orders)`, &restoObj)
	if err != nil {
		tx.Rollback()
		return 0, err
	}
	tx.Commit()
	rowsAffected, err := result.RowsAffected()
	return rowsAffected, err
}
func (resto RestoController) Update(restoObj model.Restaurant, db *sqlx.DB) (int64, error) {
	tx := db.MustBegin()
	result, err := tx.NamedExec(`UPDATE Restaurant SET manager_firstname=:manager_firstname , manager_lastname=:manager_lastname, address=:address, Postal_Code=:Postal_Code, email=:email,phone_number=:phone_number,password=:password ,country=:country, city=:city ,name=:name, created_at=:created_at,total_orders=:total_orders WHERE id= :id `, &restoObj)
	if err != nil {
		tx.Rollback()
		panic(err)
	}
	tx.Commit()
	rowsAffected, err := result.RowsAffected()
	return rowsAffected, err
}
func (resto RestoController) Delete(index int, db *sqlx.DB) (int64, error) {
	tx := db.MustBegin()
	result, err := tx.Exec(`DELETE from Restaurant WHERE id = ? `, index)
	if err != nil {
		tx.Rollback()
		return 0, err
	}
	tx.Commit()
	rowsAffected, err := result.RowsAffected()
	return rowsAffected, err
}
