package controllers

import (
	"github.com/foodie-api/go-api/model"
	"github.com/jmoiron/sqlx"
)

type UserController struct{}

func (user UserController) Find(index int, db *sqlx.DB) (model.User, error) {
	userObj := model.User{}
	err := db.Get(&userObj, "SELECT * FROM Client where id = ?", index)
	return userObj, err
}
func (user UserController) FindAll(db *sqlx.DB) ([]model.User, error) {
	userArray := []model.User{}
	err := db.Select(&userArray, "SELECT * FROM Client")
	return userArray, err
}
func (user UserController) Create(userObj model.User, db *sqlx.DB) (int64, error) {
	tx := db.MustBegin()
	result, err := tx.NamedExec(`INSERT INTO Client (firstname, lastname, email, password, created_at,orders_made) VALUES ( :firstname, :lastname,:email, :password, :created_at, :orders_made)`, &userObj)
	if err != nil {
		tx.Rollback()
		return 0, err
	}
	tx.Commit()
	rowsAffected, err := result.RowsAffected()
	return rowsAffected, err
}
func (user UserController) Update(userObj model.User, db *sqlx.DB) (int64, error) {
	tx := db.MustBegin()
	result, err := tx.NamedExec(`UPDATE Client SET firstname=:firstname , lastname=:lastname , email=:email, password=:password, created_at=:created_at, orders_made=:orders_made WHERE id= :id `, &userObj)
	if err != nil {
		tx.Rollback()
		return 0, err
	}
	tx.Commit()
	rowsAffected, err := result.RowsAffected()
	return rowsAffected, err
}
func (user UserController) Delete(index int, db *sqlx.DB) (int64, error) {
	tx := db.MustBegin()
	result, err := tx.Exec(`DELETE from Client WHERE id = ? `, index)
	if err != nil {
		tx.Rollback()
		return 0, err
	}
	tx.Commit()
	rowsAffected, err := result.RowsAffected()
	return rowsAffected, err
}
