package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/foodie-api/go-api/controllers"
	"github.com/foodie-api/go-api/model"

	"github.com/gin-gonic/gin"
)

var db = controllers.NewDB()

func main() {
	router := gin.Default()
	router.GET("client/login", clientLogin)
	router.GET("client/register", clientRegister)
	router.GET("client/update", clientUpdate)
	router.GET("client/delete", clientDelete)
	router.GET("resto/register", restoRegister)
	router.GET("resto/update", restoUpdate)
	router.GET("resto/", restoFindAll)
	http.ListenAndServe(":8080", router)
}
func clientLogin(c *gin.Context) {
	var userCont controllers.UserController
	user, err := userCont.FindAll(db)
	if err != nil {

	}
	c.JSON(200, user)
}
func clientRegister(c *gin.Context) {
	var userCont controllers.UserController
	rowsAffected, err := userCont.Create(model.User{Firstname: "Hello", Lastname: "World", Email: "sammy@hello.com", Password: "maroc123", RegistrationDate: time.Now(), OrdersMade: 0}, db)
	if err != nil {
		panic(err)
	}
	c.JSON(200, rowsAffected)
}
func clientUpdate(c *gin.Context) {
	var userCont controllers.UserController
	user, err := userCont.Find(2, db)
	user.Firstname = "HELLO WORLD"
	rowsAffected, err := userCont.Update(user, db)
	if err != nil {
		panic(err)
	}
	c.JSON(200, rowsAffected)
}
func clientDelete(c *gin.Context) {
	var userCont controllers.UserController
	rowsAffected, err := userCont.Delete(1, db)
	if err != nil {
		panic(err)
	}
	c.JSON(200, rowsAffected)
}
func restoRegister(c *gin.Context) {
	var restoCont controllers.RestoController
	rowsAffected, err := restoCont.Create(model.Restaurant{ManagerFirstname: "HELLO", ManagerLastname: "WORLD", Address: "7845 rue Niagara", PostalCode: "J4Y2N3", Email: "hello@world.com", Password: "hihihi", Country: "Canada", City: "Brossard", Name: "Klawi Juice Resto", RegistrationDate: time.Now(), TotalOrders: 0}, db)
	if err != nil {
		panic(err)
	}
	c.JSON(200, rowsAffected)
}
func restoFindAll(c *gin.Context) {
	var restoCont controllers.RestoController
	restos, err := restoCont.FindAll(db)
	if err != nil {

	}
	c.JSON(200, restos)
}
func restoUpdate(c *gin.Context) {
	var restoCont controllers.RestoController
	resto, err := restoCont.Find(3, db)
	resto.ManagerFirstname = "KLAWI JUICE"
	fmt.Printf("%+v\n", resto)
	rowsAffected, err := restoCont.Update(resto, db)
	if err != nil {
		panic(err)
	}
	c.JSON(200, rowsAffected)
}
