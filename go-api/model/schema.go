package model

import (
	"time"
)

type OptionValue struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}
type Option struct {
	ID     int64         `json:"id"`
	Name   string        `json:"name"`
	Values []OptionValue `json:"values"`
}
type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}
type RestoWorkingHour struct {
	Monday    []int32 `json:"monday"`
	Tuesday   []int32 `json:"tuesday"`
	Wednesday []int32 `json:"wednesday"`
	Thursday  []int32 `json:"thursday"`
	Friday    []int32 `json:"friday"`
	Saturday  []int32 `json:"saturday"`
	Sunday    []int32 `json:"sunday"`
}
type PromoCode struct {
	ID           int64   `json:"id"`
	Name         string  `json:"name"`
	Percentage   float64 `json:"percentage"`
	CounterUsage int64   `json:"counter_usage"`
}
type MenuItem struct {
	ID              int64         `json:"id"`
	Price           float64       `json:"price"`
	Name            string        `json:"name"`
	Description     string        `json:"description"`
	PurchaseCounter int64         `json:"purchase_counter"`
	OptionValues    []OptionValue `json:"option_values"`
}
type Menu struct {
	ID    int64      `json:"id"`
	Name  string     `json:"name"`
	Items []MenuItem `json:"items"`
}
type Order struct {
	ID               int64      `json:"id"`
	User             User       `json:"client"`
	Restaurant       Restaurant `json:"resto"`
	Total            float64    `json:"total"`
	TotalBeforeTax   float64    `json:"total_before_tax"`
	Date             time.Time  `json:"date"`
	DidReceived      bool       `json:"did_received"`
	ReceivedTime     time.Time  `json:"received_time"`
	DriverPickupTime time.Time  `json:"driver_pickup_time"`
}

type User struct {
	ID               int64     `json:"id"`
	Firstname        string    `json:"firstname"`
	Lastname         string    `json:"lastname"`
	Email            string    `json:"email"`
	Password         string    `json:"password"`
	RegistrationDate time.Time `json:"registration_date" db:"created_at"`
	OrdersMade       int       `json:"orders_made" db:"orders_made"`
	Orders           []Order   `json:"orders"`
}
type Restaurant struct {
	ID               int64            `json:"id"`
	ManagerFirstname string           `json:"manager_firstname" db:"manager_firstname"`
	ManagerLastname  string           `json:"manager_lastname" db:"manager_lastname"`
	Address          string           `json:"address"`
	PostalCode       string           `json:"postal_code" db:"Postal_Code"`
	Email            string           `json:"email"`
	PhoneNumber      string           `json:"phonenumber" db:"phone_number"`
	Password         string           `json:"password"`
	Country          string           `json:"country"`
	City             string           `json:"city"`
	Name             string           `json:"name"`
	RegistrationDate time.Time        `json:"registration_date" db:"created_at"`
	TotalOrders      int              `json:"total_orders" db:"total_orders"`
	PromoCodes       []PromoCode      `json:"promo_codes"`
	WorkingHours     RestoWorkingHour `json:"working_hours"`
	Categories       []Category       `json:"categories"`
}
