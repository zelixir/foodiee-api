# Foodiee Golang API

This is the Golang project that is served as the API.

## How to run the API on your machine 

First, you must have [Docker] installed.<br />
For Windows, go to the following [link](https://docs.docker.com/docker-for-windows/install/) to install docker on your machine<br /> 
For Mac, go to the following [link](https://docs.docker.com/docker-for-mac/install/) to install docker on your machine<br />

## Running the API 
To run the API, you will see that there is 2 bash scripts: one called start and one stop. <br />
You need to give execution access to those two files using the following command <br />
```bash
 chmod +x <name_of_script>
```
Then for running the scripts <br />
```bash
   ./<name_of_script>
```
