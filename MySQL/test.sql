-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2019-06-10 17:43:01.012
CREATE USER 'mainLogin'@'localhost' IDENTIFIED BY 'ncw7:!ZPM3A)A4"b';
GRANT ALL PRIVILEGES ON * . * TO 'mainLogin'@'localhost';
DROP DATABASE Foodiee;
CREATE DATABASE Foodiee;
USE Foodiee;
-- tables
-- Table: Category
CREATE TABLE Category (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    CONSTRAINT Category_pk PRIMARY KEY (id)
);

-- Table: Category_Resto
CREATE TABLE Category_Resto (
    restaurant_id int NOT NULL,
    category_id int NOT NULL
);

-- Table: Client
CREATE TABLE Client (
    id int NOT NULL AUTO_INCREMENT,
    email varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    firstname varchar(255) NOT NULL,
    lastname varchar(255) NOT NULL,
    created_at timestamp NOT NULL,
    orders_made int NOT NULL,
    CONSTRAINT Client_pk PRIMARY KEY (id)
);

-- Table: Extra_Menu_Item
CREATE TABLE Extra_Menu_Item (
    menu_Item_id int NOT NULL,
    extra_value_id int NOT NULL,
    price double(5,2) NOT NULL
);

-- Table: Menu
CREATE TABLE Menu (
    id int NOT NULL AUTO_INCREMENT,
    restaurant_id int NOT NULL,
    name varchar(255) NOT NULL,
    CONSTRAINT Menu_pk PRIMARY KEY (id)
);

-- Table: Menu_Item
CREATE TABLE Menu_Item (
    id int NOT NULL AUTO_INCREMENT,
    menu_id int NOT NULL,
    price double(6,2) NOT NULL,
    name varchar(255) NOT NULL,
    description text NOT NULL,
    counter_purchase int NOT NULL,
    CONSTRAINT Menu_Item_pk PRIMARY KEY (id)
);

-- Table: Options
CREATE TABLE Options (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    CONSTRAINT Options_pk PRIMARY KEY (id)
);

-- Table: Options_MenuItem
CREATE TABLE Options_MenuItem (
    menu_Item_id int NOT NULL,
    options_value_id int NOT NULL,
    price double(5,2) NOT NULL
);

-- Table: Order_Item
CREATE TABLE Order_Item (
    order_id int NOT NULL ,
    menu_item_id int NOT NULL,
    quantity int NOT NULL,
    extra_information varchar(255) NOT NULL,
    options varchar(255) NOT NULL,
    extras varchar(255) NOT NULL
);

-- Table: Orders
CREATE TABLE Orders (
    id int NOT NULL AUTO_INCREMENT,
    client_id int NOT NULL,
    resto_id int NOT NULL,
    total double(10,2) NOT NULL,
    total_beforeTax double(10,2) NOT NULL,
    order_date timestamp NOT NULL,
    order_received bool NOT NULL,
    order_received_time timestamp NOT NULL,
    order_driver_pickup_time timestamp NULL,
    CONSTRAINT Orders_pk PRIMARY KEY (id)
);

-- Table: Promo_Code
CREATE TABLE Promo_Code (
    id int NOT NULL AUTO_INCREMENT,
    restaurant_id int NOT NULL,
    promo_code varchar(255) NOT NULL,
    percentage double(4,2) NOT NULL,
    counter_usage int NOT NULL,
    CONSTRAINT Promo_Code_pk PRIMARY KEY (id)
);

-- Table: Restaurant
CREATE TABLE Restaurant (
    id int NOT NULL AUTO_INCREMENT,
    manager_firstname varchar(255) NOT NULL,
    manager_lastname varchar(255) NOT NULL,
    address varchar(255) NOT NULL,
    Postal_Code varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    phone_number varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    country varchar(255) NOT NULL,
    city varchar(255) NOT NULL,
    name varchar(255) NOT NULL,
    created_at timestamp NOT NULL,
    total_orders int NOT NULL,
    CONSTRAINT Restaurant_pk PRIMARY KEY (id)
);

-- Table: Resto_hours
CREATE TABLE Resto_hours (
    Restaurant_id int NOT NULL,
    day_of_week int NOT NULL,
    hours_json json NOT NULL
);

-- Table: extra_value
CREATE TABLE extra_value (
    id int NOT NULL AUTO_INCREMENT,
    extras_id int NOT NULL,
    name varchar(255) NOT NULL,
    CONSTRAINT extra_value_pk PRIMARY KEY (id)
);

-- Table: extras
CREATE TABLE extras (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    CONSTRAINT extras_pk PRIMARY KEY (id)
);

-- Table: options_value
CREATE TABLE options_value (
    id int NOT NULL AUTO_INCREMENT,
    options_id int NOT NULL,
    name varchar(255) NOT NULL,
    CONSTRAINT options_value_pk PRIMARY KEY (id)
);

-- foreign keys
-- Reference: Category_Resto_Category (table: Category_Resto)
ALTER TABLE Category_Resto ADD CONSTRAINT Category_Resto_Category FOREIGN KEY Category_Resto_Category (category_id)
    REFERENCES Category (id);

-- Reference: Category_Resto_Resto (table: Category_Resto)
ALTER TABLE Category_Resto ADD CONSTRAINT Category_Resto_Resto FOREIGN KEY Category_Resto_Resto (restaurant_id)
    REFERENCES Restaurant (id);

-- Reference: Extra_Menu_Item_Menu_Item (table: Extra_Menu_Item)
ALTER TABLE Extra_Menu_Item ADD CONSTRAINT Extra_Menu_Item_Menu_Item FOREIGN KEY Extra_Menu_Item_Menu_Item (menu_Item_id)
    REFERENCES Menu_Item (id);

-- Reference: Extra_Menu_Item_extra_value (table: Extra_Menu_Item)
ALTER TABLE Extra_Menu_Item ADD CONSTRAINT Extra_Menu_Item_extra_value FOREIGN KEY Extra_Menu_Item_extra_value (extra_value_id)
    REFERENCES extra_value (id);

-- Reference: Menus_Resto (table: Menu)
ALTER TABLE Menu ADD CONSTRAINT Menus_Resto FOREIGN KEY Menus_Resto (restaurant_id)
    REFERENCES Restaurant (id);

-- Reference: Opening_Hours_Restaurant (table: Resto_hours)
ALTER TABLE Resto_hours ADD CONSTRAINT Opening_Hours_Restaurant FOREIGN KEY Opening_Hours_Restaurant (Restaurant_id)
    REFERENCES Restaurant (id);

-- Reference: Options_MenuItem_Menu_Item (table: Options_MenuItem)
ALTER TABLE Options_MenuItem ADD CONSTRAINT Options_MenuItem_Menu_Item FOREIGN KEY Options_MenuItem_Menu_Item (menu_Item_id)
    REFERENCES Menu_Item (id);

-- Reference: Options_MenuItem_options_value (table: Options_MenuItem)
ALTER TABLE Options_MenuItem ADD CONSTRAINT Options_MenuItem_options_value FOREIGN KEY Options_MenuItem_options_value (options_value_id)
    REFERENCES options_value (id);

-- Reference: Orders_Client (table: Orders)
ALTER TABLE Orders ADD CONSTRAINT Orders_Client FOREIGN KEY Orders_Client (client_id)
    REFERENCES Client (id);

-- Reference: Orders_Resto (table: Orders)
ALTER TABLE Orders ADD CONSTRAINT Orders_Resto FOREIGN KEY Orders_Resto (resto_id)
    REFERENCES Restaurant (id);

-- Reference: Products_Menus (table: Menu_Item)
ALTER TABLE Menu_Item ADD CONSTRAINT Products_Menus FOREIGN KEY Products_Menus (menu_id)
    REFERENCES Menu (id);

-- Reference: Promo_Code_Resto (table: Promo_Code)
ALTER TABLE Promo_Code ADD CONSTRAINT Promo_Code_Resto FOREIGN KEY Promo_Code_Resto (restaurant_id)
    REFERENCES Restaurant (id);

-- Reference: Table_7_Orders (table: Order_Item)
ALTER TABLE Order_Item ADD CONSTRAINT Table_7_Orders FOREIGN KEY Table_7_Orders (order_id)
    REFERENCES Orders (id);

-- Reference: Table_7_Products (table: Order_Item)
ALTER TABLE Order_Item ADD CONSTRAINT Table_7_Products FOREIGN KEY Table_7_Products (menu_item_id)
    REFERENCES Menu_Item (id);

-- Reference: extra_value_extras (table: extra_value)
ALTER TABLE extra_value ADD CONSTRAINT extra_value_extras FOREIGN KEY extra_value_extras (extras_id)
    REFERENCES extras (id);

-- Reference: options_value_Options (table: options_value)
ALTER TABLE options_value ADD CONSTRAINT options_value_Options FOREIGN KEY options_value_Options (options_id)
    REFERENCES Options (id);

-- End of file.

